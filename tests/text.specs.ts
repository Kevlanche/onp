import {diffText} from "../src/index";

describe("onp", () => {

	describe("text diff", () => {

		it("check same text", () => {
			const data = diffText("Hello how are you?", "Hello how are you?");

			expect(data.distance).toBe(0);
			expect(data.lcs).toBe("Hello how are you?");
			expect(data.results).toEqual([
				{  left: 'Hello how are you?', right: 'Hello how are you?', state: 0 }
			]);
		});

		it("example test", () => {
			const data = diffText("Test A", "Test B");

			expect(data.distance).toBe(2);
			expect(data.lcs).toBe("Test ");
			expect(data.results).toEqual([
				{ left: 'Test ', right: 'Test ', state: 0 },
				{ left: 'A', right: 'A', state: -1 },
				{ left: 'B', right: 'B', state: 1 }
			]);
		});

		it("check slightly different text", () => {
			const data = diffText("Hello how are you?", "Hello huw are you?");

			expect(data.distance).toBe(2);
			expect(data.lcs).toBe("Hello hw are you?");
			expect(data.results).toEqual([
				{ left: 'Hello h', right: 'Hello h', state: 0 },
				{ left: 'o', right: 'o', state: -1 },
				{ left: 'u', right: 'u', state: 1 },
				{ left: 'w are you?', right: 'w are you?', state: 0 }
			]);
		});

		it("check more different text with some new words", () => {
			const data = diffText("Hello huw are you?", "Hello man how are you today?");

			expect(data.distance).toBe(12);
			expect(data.lcs).toBe("Hello hw are you?");
			expect(data.results).toEqual([
				{ left: 'Hello ', right: 'Hello ', state: 0 },
				{ left: 'man ', right: 'man ', state: 1 },
				{ left: 'h', right: 'h', state: 0 },
				{ left: 'o', right: 'o', state: 1 },
				{ left: 'u', right: 'u', state: -1 },
				{ left: 'w are you', right: 'w are you', state: 0 },
				{ left: ' today', right: ' today', state: 1 },
				{ left: '?', right: '?', state: 0 }
			]);
		});

		it("check more totally different text", () => {
			const data = diffText("Hello how are you?", "Today is a good day!");

			expect(data.distance).toBe(26);
			expect(data.lcs).toBe("o o ay");
			expect(data.results).toEqual([
				{left: "T", right: "T", state: 1},
				{left: "Hell", right: "Hell", state: -1},
				{left: "o", right: "o", state: 0},
				{left: "day", right: "day", state: 1},
				{left: " ", right: " ", state: 0},
				{left: "is a g", right: "is a g", state: 1},
				{left: "h", right: "h", state: -1},
				{left: "o", right: "o", state: 0},
				{left: "od", right: "od", state: 1},
				{left: "w", right: "w", state: -1},
				{left: " ", right: " ", state: 0},
				{left: "d", right: "d", state: 1},
				{left: "a", right: "a", state: 0},
				{left: "re ", right: "re ", state: -1},
				{left: "y", right: "y", state: 0},
				{left: "!", right: "!", state: 1},
				{left: "ou?", right: "ou?", state: -1}
			]);
		});

		it("check 'The men are bad. I hate the men' and 'The men are bad. John likes the men. I hate the men'", () => {
			const data = diffText("The men are bad. I hate the men", "The men are bad. John likes the men. I hate the men");

			expect(data.distance).toBe(20);
			expect(data.lcs).toBe("The men are bad. I hate the men");
			expect(data.results).toEqual([
				{left: "The men are bad. ", right: "The men are bad. ", state: 0},
				{left: "John likes the men. ", right: "John likes the men. ", state: 1},
				{left: "I hate the men", right: "I hate the men", state: 0}
			]);
		});

	});

});